\documentclass{article}
\usepackage[francais]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsfonts}
\usepackage{graphicx}
\usepackage{bbm}
\usepackage{subfig}
\usepackage[hmarginratio=1:1,top=32mm,columnsep=20pt]{geometry}
\usepackage{multirow}
\usepackage{multicol} % Style double colonne
\usepackage{abstract} % Customization de l'abstract
\usepackage{fancyhdr} % en-têtes et pieds de page
\usepackage{float} % Nécessaire pour les tables et figures dans l'environnement double colonne
\usepackage{array}

\usepackage[colorlinks=true,linkcolor=red,urlcolor=blue,filecolor=green]{hyperref} % hyperliens

% En-têtes et pieds de page
\pagestyle{fancy}
\fancyhead{} % Blank out the default header
\fancyfoot{} % Blank out the default footer
\fancyhead[C]{SY09 - TP3 \hfill Aude COLLEVILLE - Alexis MATHEY } % Custom header text
\fancyfoot[RO,LE]{\thepage} % Custom footer text

%\setlength{\parskip}{1ex} % espace entre paragraphes

\newcommand{\bsx}{\boldsymbol{x}}
\newcommand{\transp}{^{\mathrm{t}}}

\newcolumntype{C}[1]{>{\centering\arraybackslash}m{#1}}


%----------------------------------------------------------------------------------------

\title{Discrimination, théorie bayésienne de la décision}

\author{Alexis Mathey \& Aude Colleville}
\date{\today}

%----------------------------------------------------------------------------------------

\begin{document}

\maketitle % Insert title

\thispagestyle{fancy} % All pages have headers and footers


%----------------------------------------------------------------------------------------


\section{Classifieur euclidien, K plus proches voisins}
Dans cet exercice, on souhaite étudier les performances du classifieur euclidien et des K plus proches voisins sur différents jeux de données binaires dont les individus sont répartis en deux classes. Pour cela, nous avons tout d'abord créé plusieurs fonctions permettant de définir ces classifieurs:
\begin{itemize}
\item \texttt{ceuc.app} s'occupe de l'apprentissage des paramètres du classifieur euclidien.
\item \texttt{ceuc.val} permet le classement d’un tableau individus-variables \texttt{Xtst} à partir de la valeur du classificateur euclidien retourné par \texttt{ceuc.app}.
\item \texttt{kppv.tune} détermine le nombre « optimal » de voisins \texttt{Kopt}, c’est-à-dire donnant les meilleurs performances sur un ensemble de validation étiqueté.
\item \texttt{kppv.val} permet le classement d’un tableau individus-variables \texttt{Xtst} par la méthode des K plus proches voisins.
\end{itemize}

\subsection{Test des fonctions}
Afin de tester nos fonctions et de visualiser les frontières de décisions, nous utilisons les fonctions \texttt{front.ceuc} et \texttt{front.kppv} sur le jeu de données Synth1-40.
\begin{figure}[H]
	\centering
	\parbox{7cm}{
		\includegraphics[width=6cm]{img/frontCeuc40.png}
		\caption{Frontière de décision du jeu de données à l'aide du classifieur euclidien.}
		\label{fig:acp_iris_1}}
	\qquad
	\begin{minipage}{7cm}
		\includegraphics[width=6cm]{img/frontKppv40.png}
		\caption{Frontière de décision du jeu de données à l'aide des K plus proches voisins.}
		\label{fig:acp_iris_1}
	\end{minipage}
\end{figure}


\subsection{Évaluation des performances}
Afin d'évaluer les performances des classifieurs, on cherche à estimer le taux d'erreur $\epsilon$ de celui-ci.
Pour ce faire, on répète 20 fois une séparation aléatoire pour former un ensemble d'apprentissage et un ensemble de test et de manière optionnelle, un ensemble de validation sur 5 jeux de données différents.
On peut ensuite, pour chaque séparation, apprendre les paramètres du modèle sur l'ensemble d'apprentissage formé et calculer le taux d'erreur obtenu sur l'ensemble de test. 
On peut ainsi recueillir un échantillon de 20 estimations du taux d'erreur et alors déterminer une estimation ponctuelle de $\epsilon$ (moyenne) et un intervalle de confiance sur $\epsilon$.

\subsubsection{Jeux de données Synth1-40, Synth1-100, Synth1-500 et Synth1-1000}
On commence par étudier les quatre jeux de données de même distribution : Synth1-40, Synth1-100, Synth1-500 et Synth1-2000.

Dans un premier temps, on estime pour chacun les paramètres $\mu_{k}$ et $\Sigma_{k}$ des distributions conditionnelles, ainsi que les proportions $\pi_{k}$ des classes.
{\renewcommand{\arraystretch}{1.2}%
\begin{table}[H]
\begin{center}
\begin{tabular}{|l|c|c|}
  \hline
  Paramètre&Classe 1&Classe 2\\
  \hline
 $\mu_{k}$ &$\begin{pmatrix}
   -0.04917257 & 1.97600535 \end{pmatrix}$ &$\begin{pmatrix}
   0.1098999 & -0.6960198 \end{pmatrix}$ \\
  $\Sigma_{k}$&$\begin{pmatrix}
   1.449030 & -0.1082540 \\
   -0.108254 & 0.8941746 
\end{pmatrix}$ & $\begin{pmatrix}
   1.7491868 & -0.3228813 \\
   -0.3228813 & 0.8320192 
\end{pmatrix}$\\
  $\pi_{k}$&$0.575$&$0.425$\\
  \hline
\end{tabular}
\end{center}
\caption{Estimation des paramètres pour le jeu de données Synth1-40.}
\label{table:Param_synth1-40}
\end{table}

\begin{table}[H]
\begin{center}
\begin{tabular}{|l|c|c|}
  \hline
  Paramètre&Classe 1&Classe 2\\
  \hline
 $\mu_{k}$ &$\begin{pmatrix}
   0.168409 & 2.059974 \end{pmatrix}$ &$\begin{pmatrix}
   -0.1872786 & -1.0635861 \end{pmatrix}$ \\
  $\Sigma_{k}$&$\begin{pmatrix}
   0.89237700 & 0.07076995 \\
   0.07076995 & 0.88700525 
\end{pmatrix}$ & $\begin{pmatrix}
   0.9004231 & 0.0694818 \\
   0.0694818 & 0.6118969
\end{pmatrix}$\\
  $\pi_{k}$&$0.46$&$0.54$\\
  \hline
\end{tabular}
\end{center}
\caption{Estimation des paramètres pour le jeu de données Synth1-100.}
\label{table:Param_synth1-100}
\end{table}

\begin{table}[H]
\begin{center}
\begin{tabular}{|l|c|c|}
  \hline
  Paramètre&Classe 1&Classe 2\\
  \hline
 $\mu_{k}$ &$\begin{pmatrix}
   0.02752285 & 2.00367384 \end{pmatrix}$ &$\begin{pmatrix}
   -0.03351599 & -0.90136250 \end{pmatrix}$ \\
  $\Sigma_{k}$&$\begin{pmatrix}
   0.994104398 & 0.003648037 \\
   0.003648037 & 0.943428380 
\end{pmatrix}$ & $\begin{pmatrix}
   0.89852542 & -0.02292929\\
   -0.02292929 & 0.81674861
\end{pmatrix}$\\
  $\pi_{k}$&$0.516$&$0.484$\\
  \hline
\end{tabular}
\end{center}
\caption{Estimation des paramètres pour le jeu de données Synth1-500.}
\label{table:Param_synth1-500}
\end{table}

\begin{table}[H]
\begin{center}
\begin{tabular}{|l|c|c|}
  \hline
  Paramètre&Classe 1&Classe 2\\
  \hline
 $\mu_{k}$ &$\begin{pmatrix}
   -0.08398649 & 1.98475710 \end{pmatrix}$ &$\begin{pmatrix}
   -0.03848676 & -1.01335894 \end{pmatrix}$ \\
  $\Sigma_{k}$&$\begin{pmatrix}
   1.02977969 & 0.05372482 \\
   0.05372482 & 1.01580164 
\end{pmatrix}$ & $\begin{pmatrix}
   0.97700294 & -0.01877442\\
   -0.01877442 & 0.95460230
\end{pmatrix}$\\
  $\pi_{k}$&$0.481$&$0.519$\\
  \hline
\end{tabular}
\end{center}
\caption{Estimation des paramètres pour le jeu de données Synth1-1000.}
\label{table:Param_synth1-1000}
\end{table}}

D'après les résultats ci-dessus, on remarque que les matrices $\Sigma$ sont globalement de la forme $\begin{pmatrix}
   1 & 0\\
   0 & 1
\end{pmatrix}$ et que nos proportions sont très proche de 0,5. Ainsi, on peut conclure que les nuages de points sont approximativement sphériques et de même volume.

Par la suite, une fois le calcul du taux d'erreur de test et d'apprentissage pour les 20 séparations aléatoires, on peut calculer l'estimation ponctuelle du taux d'erreur $\epsilon$ ainsi qu'un intervalle de confiance (calculé pour un seuil de 95\%) à partir des estimations faites sur l'ensemble d'apprentissage et celles faites sur l'ensemble de test.
{\renewcommand{\arraystretch}{1.2}%
\begin{table}[H]
\begin{center}
\begin{tabular}{|l|c|c|c|c|}
  \hline
  &Synth1-40&Synth1-100&Synth1-500&Synth1-1000\\
  \hline
  \hline
 $\epsilon$ ensemble d'app &0.07115&0.047015&0.05075&0.06672 \\
 IC ensemble d'app&[0.05542,0.08689]& [0.04129,0.05274]&[0.04776,0.05374] & [0.06421,0.06923]\\
 \hline
 $\epsilon$ ensemble de test &0.08571&0.07273&0.05060&0.06456 \\
  IC ensemble de test&[0.04691,0.12452]&[0.06024,0.08522]& [0.04582,0.05538]&[0.05938,0.06975]\\
  \hline
\end{tabular}
\end{center}
\caption{Estimation ponctuelle du taux d'erreur $\epsilon$ et intervalle de confiance par le classifieur Euclidien.}
\label{table:Estim_synth1}
\end{table}}

On remarque que les résultats sont très semblables que ce soit pour l'ensemble d'apprentissage ou de test. L'estimation ponctuelle du taux d'erreur $\epsilon$ est très petite et l'intervalle de confiance est également petit. Ainsi, on peut en conclure que l'on obtient de bons résultats. Ceci était assez prévisible puisque le classifieur euclidien fournit de bons résultats pour des nuages de points approximativement sphérique et de même volume.\\

Par ailleurs, si l'on effectue une séparation aléatoire d'un l'ensemble de données (n'importe lequel des jeux de données Synth1) en un ensemble d'apprentissage et un ensemble de test et que l'on chercher à déterminer le nombre optimal de voisins à l'aide de la fonction kppv.tune, en utilisant l'ensemble d'apprentissage comme ensemble de validation, on remarque que l'on obtient à chaque fois 1. Ceci s'explique par le fait que nous prenons un ensemble d'apprentissage comme ensemble de validation, ce qui signifie que l'algorithme va choisir un voisin le plus proche à savoir lui même, et on obtiendra par conséquent 100\% de fiabilité.\\

Finalement, nous reproduisons l'exercice précédent en effectuant 20 séparations aléatoires sur les mêmes jeux de données, en un ensemble d'apprentissage, un ensemble de validation et un ensemble de test afin de tester les performances du classifieur des K plus proches voisins. Pour chaque séparation, on cherche à définir le nombre optimal de voisins à partir de l'ensemble de validation et à calculer le taux d'erreur sur l'ensemble d'apprentissage et sur l'ensemble de test. Comme précédemment, nous déterminons les estimations ponctuelles du taux d'erreur et les intervalles de confiance sur ces deux ensembles (Table \ref{table:estim_synth1_kppv}).

{\renewcommand{\arraystretch}{1.2}%
\begin{table}[H]
	\begin{center}
		\begin{tabular}{|l|c|c|c|c|}
			\hline
			&Synth1-40&Synth1-100&Synth1-500&Synth1-1000\\
			\hline
			\hline
			$\epsilon$ ensemble d'app &0.0775&0.018&0.044&0.05620000 \\
			IC ensemble d'app&[-0.02088, 0.17588]&[0.00631, 0.02969]&[0.03764, 0.05036]&[0.05225, 0.06015]\\
			\hline
			$\epsilon$ ensemble de test &0.325&0.15&0.0702381&0.078 \\
			IC ensemble de test&[0.17938, 0.47062]&[0.02191, 0.27809]&[0.06192, 0.07855 ]&[0.07045, 0.08675]\\
			\hline
		\end{tabular}
	\end{center}
	\caption{Estimation ponctuelle du taux d'erreur $\epsilon$ et intervalle de confiance par les K plus proches voisins ($K \leq 10$).}
	\label{table:estim_synth1_kppv}
\end{table}}
	Tout d'abord, on peut remarquer que les taux d'erreurs sur l'ensemble de test sont bien plus élevés que ceux sur l'ensemble d'apprentissage. Ceci s'explique par le fait que l'algorithme des k plus proches voisins calculent les distances avec ses k plus proches voisins dont lui-même pour l'ensemble d'apprentissage.
Ces résultats ont été obtenus en ne considérant que des $K \leq 10$. On peut remarquer que, par cette méthode, l'erreur pour l'ensemble de test sur les petits jeux de données est bien plus importante qu'avec la méthode du classificateur euclidien. Néanmoins, sur les plus grands jeux de données, l'erreur descend fortement et passe sous la barre des 10\%.

Pour les grands jeux de données, si on augmente la valeur maximale de $K$, la proportion d'erreur diminue fortement (voir Table \ref{table:Estim_synth2_kppv}). Néanmoins, cette amélioration de la précision se fait au prix des performances car le calcul est alors beaucoup plus long.

\subsubsection{Jeu de données Synth2-1000}
Nous considérons désormais le jeu de données Synth2-100, qui a une distribution différente des autres jeux de données étudiés précédemment. On estime tout d'abord les paramètres $\mu_{k}$ et $\Sigma_{k}$ ainsi que les proportions $\pi_{k}$ des classes.
{\renewcommand{\arraystretch}{1.2}%
\begin{table}[H]
\begin{center}
\begin{tabular}{|l|c|c|}
  \hline
  Paramètre&Classe 1&Classe 2\\
  \hline
 $\mu_{k}$ &(-0.07240,2.94471) &(-0.09795,-5.10133) \\
  $\Sigma_{k}$&$\begin{pmatrix}
   1.06923 & -0.05316 \\
   -0.05316  & 1.06883 
\end{pmatrix}$ & $\begin{pmatrix}
   5.21695  & 0.11433 \\
   0.11433  & 5.16915 
\end{pmatrix}$\\
  $\pi_{k}$&$0.486$&$0.514$\\
  \hline
\end{tabular}
\end{center}
\caption{Estimation des paramètres pour le jeu de données Synth2-1000.}
\label{table:Param_synth2-1000}
\end{table}}
Nous remarquons cette fois-ci encore qu'il s'agit de nuages de points relativement sphériques et de même volume avec cependant le nuage de points de la classe 2 qui est plus allongé que celui de la classe 1. Les valeurs de $\mu_{k}$ nous permettent également de remarquer qu'ils sont plus éloignés l'un de l'autre.

Nous calculons donc de nouveau les estimations ponctuelles et intervalles de confiance sur l'ensemble d'apprentissage et de test.
{\renewcommand{\arraystretch}{1.2}%
\begin{table}[H]
	\begin{center}
		\begin{tabular}{|l|c|}
		  \hline
		  &Synth2-1000\\
		  \hline
		 $\epsilon$ ensemble d'app &0.02241 \\
		 IC ensemble d'app&[0.02061,0.02422]\\
		 $\epsilon$ ensemble de test &0.02012 \\
		  IC ensemble de test&[0.01637,0.02387]\\
		  \hline
		\end{tabular}
	\end{center}
\caption{Estimation ponctuelle du taux d'erreur $\epsilon$ et intervalle de confiance par le classifieur Euclidien.}
\label{table:Estim_synth2}
\end{table}}
{\renewcommand{\arraystretch}{1.2}%
	\begin{table}[H]
		\begin{center}
			\begin{tabular}{|l|c|}
				\hline
				&Synth2-1000\\
				\hline
				$\epsilon$ ensemble d'app &0.0032 \\
				IC ensemble d'app&[0.001766763, 0.004633237 ]\\
				$\epsilon$ ensemble de test &0.0084 \\
				IC ensemble de test&[0.00573833, 0.011061665]\\
				\hline
			\end{tabular}
		\end{center}
		\caption{Estimation ponctuelle du taux d'erreur $\epsilon$ et intervalle de confiance par la méthode des K plus proches voisins ($K \leq 100$).}
		\label{table:Estim_synth2_kppv}
	\end{table}}
Nous constatons que les estimations ponctuelles et les intervalles de confiance sont encore plus petits que précédemment. Les deux nuages étant plus éloignés l'un de l'autre, ce résultat n'est pas surprenant.

\section{Règle de Bayes}
\subsection{Distributions marginales}
Nous souhaitons calculer les distributions marginales des variables $X^{1}$ et $X^{2}$ dans chaque classe. 
On a donc les équations suivantes:
\begin{equation}
\begin{split}
f_{1}(x_{1},x_{2}) =  \dfrac{1}{2\pi} \exp (-\dfrac{1}{2}(x_{1}^{2} + (x_{2}-2)^{2})) \\
f_{1}(x_{1}) = \dfrac{1}{2\pi} \exp (-\dfrac{1}{2}x_{1}^{2})\\
f_{1}(x_{2}) = \dfrac{1}{2\pi} \exp (-\dfrac{1}{2}(x_{2}-2)^{2})\\
\end{split}
\end{equation}
On sait que les variables sont indépendantes dont on peut écrire la loi jointe comme le produit des lois marginales.
Or, on sait que la densité de probabilité de la loi normale est donnée par :
\begin{equation}
f(x) = \dfrac{1}{\sigma \sqrt{2\pi}} \exp (-\dfrac{1}{2}(\dfrac{x-\mu}{\sigma})^{2})
\end{equation}
On obtient donc pour la classe 1 des données Synth1 $X^{1}_{1}\sim N(0,1)$ et $X^{2}_{1}\sim N(2,1)$.
De la même façon, pour la classe 2, on obtient $X^{1}_{2}\sim N(0,1)$ et $X^{2}_{2}\sim N(-1,1)$

Pour les données Synth2, on peut appliquer le même raisonnement et obtenir :
$X^{1}_{1}\sim N(0,\sqrt{5})$ et $X^{2}_{1}\sim N(3,\sqrt{5})$ pour la classe 1 et $X^{1}_{1}\sim N(0,\sqrt{5})$ et $X^{2}_{1}\sim N(-5,\sqrt{5})$ pour la classe 2
\subsection{Courbes d'iso-densité}
Les courbes d'iso-densité sont telles que $f_{1}(x) = K_{1}$ et $f_{2}(x) = K_{2}$ avec $K_{1}$ et $K_{2}$ des constantes.
On a donc les équations suivantes pour les jeux de données Synth1:
\begin{equation}
\begin{split}
f_{1}(x)=\dfrac{1}{2\pi}*\exp(-\dfrac{1}{2}(x_{1}^{2}+(x_{2}-2)^{2})=K_{1}\\
-\dfrac{1}{2}(x_{1}^{2}+(x_{2}-2)^{2}) = \ln( 2\pi K_{1})\\
x_{1}^{2}+(x_{2}-2)^{2}=-2\ln(2\pi K_{1})\\
f_{2}(x)=\dfrac{1}{2\pi}*\exp(-\dfrac{1}{2}(x_{1}^{2}+(x_{2}+1)^{2}))=K_{2}\\
x_{1}^{2}+(x_{2}+1)^{2}=-2\ln(2\pi K_{2})
\end{split}
\end{equation}

On obtient bien des courbes d'iso-densité qui représentent des cercles de rayon $\sqrt{-2\ln(2\pi K_{1})}$ et de centre (0,2) pour la classe 1 et de rayon $\sqrt{-2\ln(2\pi K_{2})}$ et de centre (0,-1) pour la classe 2.

Pour le jeu de données Synth2-1000, de la même manière:
\begin{equation}
\begin{split}
f_{1}(x)=\dfrac{1}{10\pi}*\exp(-\dfrac{1}{10}(x_{1}^{2}+(x_{2}-3)^{2})=K_{1}\\
-\dfrac{1}{10}(x_{1}^{2}+(x_{2}-3)^{2}) = \ln( 10\pi K_{1})\\
x_{1}^{2}+(x_{2}-3)^{2}=-10\ln(10\pi K_{1})\\
f_{2}(x)=\dfrac{1}{10\pi}*\exp(-\dfrac{1}{10}(x_{1}^{2}+(x_{2}+5)^{2}))=K_{2}\\
x_{1}^{2}+(x_{2}+5)^{2}=-10\ln(10\pi K_{2})
\end{split}
\end{equation}

On obtient donc un cercle de rayon $\sqrt{-10\ln(10\pi K_{1})}$ et de centre (0,3) pour la classe 1, et un cercle de rayon $\sqrt{-10\ln(10\pi K_{2})}$ et de centre (0,-5) pour la classe 2.

\subsection{Expression de la règle de Bayes}
Pour le problème de discrimination des classes $\omega_{1}$ et $\omega_{2}$, nous déterminons la règle de Bayes. 

Par définition, la règle de Bayes nous donne :
\begin{equation*}
	\delta^{*} =
	\left\{\begin{array}{ll}
	\omega_{1} & \mbox{ si P($\omega_{2} | x$) < P($\omega_{1} | x$)}\\
	\omega_{2} & \mbox{ sinon}\\
	\end{array}\right.
\end{equation*}
Dans le cas de deux classes de probabilités a priori $\pi_{1}$ et $\pi_{2}$, cette équation devient :
\begin{equation*}
\delta^{*} =
\left\{\begin{array}{ll}
\omega_{1} & \mbox{ si $\frac{f_{1}(x)}{f_{2}(x)} > \frac{\pi_{2}}{\pi_{1}}$ }\\
\omega_{2} & \mbox{ sinon}\\
\end{array}\right.
\end{equation*}
Pour les quatre premiers jeu de données, les deux classes suivent des distributions de même $\Sigma$. Nous sommes donc dans le cas de l'analyse discriminante linéaire. Les fonctions discriminantes sont donc de la forme :
\begin{equation*}
	h_{k}(x) = \left(\Sigma^{-1} \mu_{k}\right)^{T} x - \frac{1}{2}\mu_{k}^{T} \Sigma^{-1} \mu_{k} + \ln \pi_{k}
\end{equation*} 

Pour le dernier jeu, nous ne pouvons pas faire cette hypothèse. Nous sommes donc dans le cas de l'analyse discriminante quadratique et les fonctions discriminantes sont de la forme :
\begin{equation*}
	g_{k}(x) = -\frac{1}{2}(x - \mu_{k})^{T} \Sigma_{k}^{-1} (x - \mu_{k}) - \frac{1}{2} \ln(\det\Sigma_{k})) + \ln \pi_{k} - \frac{p}{2} \ln(2\pi)
\end{equation*}

\subsection{Frontières de décision}
À partir de son expression de la règle de Bayes, il nous est possible de déterminer la frontière de décision pour le 1\textsuperscript{er} jeu de données. La frontière de décision est l'ensemble des points solution de l'équation suivante :
\begin{equation*}
	h_{1}(x) = h_{2}(x)
\end{equation*}

Comme nous sommes dans le cas de l'analyse discriminante linéaire, cette équation se traduit par :
\begin{equation}
	\left(\Sigma^{-1} (\mu_{1} - \mu{2})\right)^{T} \left(s - \dfrac{\mu_{1} - \mu_{2}}{2} + \dfrac{\ln(\pi_{1}/\pi_{2})}{(\mu_{1} - \mu_{2})^{T} \Sigma^{-1} (\mu_{1}-\mu_{2})}(\mu_{1} - \mu_{2})\right) = 0
\label{eq:front_decision}
\end{equation}
Comme $\pi_{1} = \pi_{2}$, nous avons $\ln(\pi_{1}/\pi_{2}) = 0$. 
\begin{equation*}
	(\ref{eq:front_decision}) \Leftrightarrow \left(\Sigma^{-1} (\mu_{1} - \mu{2})\right)^{T} \left(s - \dfrac{\mu_{1} - \mu_{2}}{2}\right) = 0
\end{equation*}

La résolution de cette équation avec $\mu_{1} = \begin{pmatrix}0\\2\end{pmatrix}$, $\mu_{2} = \begin{pmatrix}0\\-1\end{pmatrix}$ et $\Sigma = \begin{pmatrix}1&0\\0&1\end{pmatrix}$ nous donne l'équation $y = \frac{1}{2}$ pour la frontière de décision pour le premier jeu de données.

Cette équation est la même pour les 3 jeux de données suivants car ils suivent la même distribution que le premier.\\

Les Figures \ref{fig:frontiere_synth1_40} à \ref{fig:frontiere_synth1_1000} présentent le tracé de cette frontière sur les quatre premiers jeux de données.

\begin{figure}
	\centering
	\parbox{7cm}{
		\includegraphics[width=6cm]{img/front_decision_40.png}
		\caption{Jeu de données \texttt{Synth1-40} avec la frontière de décision $y = \frac{3}{4}$}
		\label{fig:frontiere_synth1_40}}
	\qquad
	\begin{minipage}{7cm}
		\includegraphics[width=6cm]{img/front_decision_100.png}
		\caption{Jeu de données \texttt{Synth1-100} avec la frontière de décision $y = \frac{3}{4}$}
		\label{fig:frontiere_synth1_100}
	\end{minipage}\\
	\begin{minipage}{7.5cm}
		\includegraphics[width=6cm]{img/front_decision_500.png}
		\caption{Jeu de données \texttt{Synth1-500} avec la frontière de décision $y = \frac{3}{4}$}
		\label{fig:frontiere_synth1_500}
	\end{minipage}
	\begin{minipage}{7.5cm}
		\includegraphics[width=6cm]{img/front_decision_1000.png}
		\caption{Jeu de données \texttt{Synth1-1000} avec la frontière de décision $y = \frac{3}{4}$}
		\label{fig:frontiere_synth1_1000}
	\end{minipage}
\end{figure}

\subsection{Erreur de Bayes}
Nous sommes dans le cas de deux classes de même matrice de covariance $\Sigma$ et de mêmes probabilités a priori $\pi_{1}=\pi_{2}$. La probabilité d'erreur de Bayes se résume à la formule suivante :
\begin{equation*}
	\epsilon^{*} = \phi\left(-\dfrac{\Delta}{2}\right)
\end{equation*}
avec : 
\begin{equation*}
	\Delta^{2} = (\mu_{2} - \mu_{1})^{T}\Sigma^{-1}(\mu_{2} - \mu_{1})
\end{equation*}
Avec les paramètres des premiers jeux de données, nous obtenons $\Delta = 3$ et donc :
\begin{equation*}
\epsilon^{*} = 0,93
\end{equation*}
Le taux d'erreur de Bayes est donc de $0,07$ pour les quatre premiers jeux de données.


\section{Conclusion}
Ce TP nous a donc permis de mettre en application les concepts de la théorie Bayésienne de la décision et de l'analyse discriminante à l'aide des outils de R. Nous avons mis en évidence les différences entre deux des classifieurs les plus connus : le classifieur euclidien et les K plus proches voisins. Le premier étant particulièrement adapté au cas où les deux classes suivent la même distribution, le classifieur nous donnant alors une frontière de décision linéaire. Le second est en revanche plus flexible car il peut permettre de tracer des frontières non linéaires mais demande plus d'étapes de calcul.

Dans la deuxième partie nous avons même pu relier ces observation à la théorie mathématique de Bayes et déterminer des frontières de décision simples sans l'aide des outils R.\\

Nous avons donc bien mis en évidence le fait qu'il faille avoir une très bonne connaissance des données que nous voulons classifier afin d'être capable de choisir le classifieur le plus adapté.

%----------------------------------------------------------------------------------------


\end{document}
