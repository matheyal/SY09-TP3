donn40 <- read.table("data/Synth1-40.txt", header=F)
X40 <- donn40[,1:2]
z40 <- donn40[,3]
Xapp<-X40[c(1:15,24:35),]
zapp<-z40[c(1:15,24:35)]
Xtst<-X40[c(16:20,36:40),]
ztst<-z40[c(16:20,36:40)]

donn100 <- read.table("data/Synth1-100.txt", header=F)
X100 <- donn100[,1:2]
z100 <- donn100[,3]

donn500 <- read.table("data/Synth1-500.txt", header=F)
X500 <- donn500[,1:2]
z500 <- donn500[,3]

donn1000 <- read.table("data/Synth1-1000.txt", header=F)
X1000 <- donn1000[,1:2]
z1000 <- donn1000[,3]

donn2_1000 <- read.table("data/Synth2-1000.txt", header=F)
X2_1000 <- donn2_1000[,1:2]
z2_1000 <- donn2_1000[,3]

X<-X40
z<-z40
